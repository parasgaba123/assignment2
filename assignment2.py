from flask import Flask, render_template, request, redirect, url_for, jsonify, json
import random
app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON')
def bookJSON():
	r = json.dumps(books)
	return r

@app.route('/', methods = ['GET', 'POST'])
@app.route('/book/', methods=['GET', 'POST'])
def showBook():
	num = len(books)

	return render_template("showBook.html",books = books, num=num)



@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	dum = len(books)
	if request.method == "POST":
		bn = request.form['name']
		books.append({'title': bn, 'id': str(dum+1) })
		return redirect(url_for("showBook"))

	else:
		return render_template("newBook.html")


@app.route('/book/<string:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	bookstr = str(book_id)
	if request.method == "POST":
		for k in range(len(books)):
			books[k]['id'] = str(k+1)
		bookname = request.form['ename']
		for x in books:
			if x['id'] == bookstr:
				x['title'] = bookname
		return redirect(url_for("showBook"))

	else:
		return render_template("editBook.html", book_id = bookstr)



@app.route('/book/<string:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	book_str = str(book_id)
	if request.method == "POST":
		for j in books:
			if j['id'] == book_str:
				break
		books.remove(j)

		return redirect(url_for("showBook"))
	else:
		return render_template("deleteBook.html", book_id = book_id)


if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
	
